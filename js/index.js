$(function () {
  			$('.carousel').carousel({
  				interval: 1500
  			});
  			$('#Registro').on('show.bs.modal', function (e) {
				  console.log('El modal se esta mostrando');
          $('#regbtn').removeClass('btn-info');
          $('#regbtn').addClass('btn-outline-secondary');
          $('#regbtn').prop('disabled',true);
				});
        $('#Registro').on('shown.bs.modal', function (e) {
          console.log('El modal se ha mostrado');
        });
        $('#Registro').on('hide.bs.modal', function (e) {
          console.log('El modal se esta cerrando');
          $('#regbtn').removeClass('btn-outline-secondary');
          $('#regbtn').addClass('btn-info');
          $('#regbtn').prop('disabled',false);
        });
        $('#Registro').on('hidden.bs.modal', function (e) {
          console.log('El modal se ha cerrado');
        });
        $('[data-toggle="popover"]').popover();
        $("[data-toggle='tooltip']").tooltip();
});
